'use strict';

window.$ = window.jQuery = require('jquery/dist/jquery.js');

require('slick-carousel/slick/slick.js');

//require('bootstrap-sass/assets/javascripts/bootstrap/affix.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/alert.js');
require('bootstrap-sass/assets/javascripts/bootstrap/button.js');
require('bootstrap-sass/assets/javascripts/bootstrap/carousel.js');
require('bootstrap-sass/assets/javascripts/bootstrap/collapse.js');
require('bootstrap-sass/assets/javascripts/bootstrap/dropdown.js');
require('bootstrap-sass/assets/javascripts/bootstrap/modal.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/popover.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js');
require('bootstrap-sass/assets/javascripts/bootstrap/tab.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/tooltip.js');
require('bootstrap-sass/assets/javascripts/bootstrap/transition.js');

$(document).on('ready', function () {
    
      
  // Slick carousel init
  $('.slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  
  // Bootstrap tabs init
  $('.nav-tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
  
  // Bootstrap accordion init
  var accordions  = $('.panel-group'),
      activeClass = 'panel-active';
  
  $(accordions).find(' > .panel').on('show.bs.collapse', function (e) {
    accordions.find('.panel-heading').removeClass(activeClass);
    $(this).find('.panel-heading').addClass(activeClass);
  });

  // Bootstrap tabs init
  var tabs = $('.tabs .tab');
  $(tabs).on('click', function (e) {
    e.preventDefault();

    var siblingTabs = $(this).closest('.tabs').find('.tab');
    
    siblingTabs.removeClass('active').find('a').each(function (i, el) {
      $($(el).attr('href')).hide();
    });

    $($(this).find('a').attr('href')).show();
    $(this).addClass('active');
  });
  
  // Yamm init
  $('.yamm .dropdown-menu').on('click', function (e) {
    e.stopPropagation();
  });
});
